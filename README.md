# Groupe 3

### Membre 
* Aleksandr Vassilyev
* Claudian Camus
* Quentin Costes
* Julie Bonnefond

## Description

Une API sur l'éligibilité aux aides durant la périodes du confinement due au Covid-19

## Install

Clone and initialize the repository.

Clone with HTTPS: https://gitlab.com/esn-81/industrialisation/projets/eligibilite
```
git clone https://gitlab.com/esn-81/industrialisation/projets/eligibilite.git
cd eligibilite
touch README.md
git add README.md
git commit -m "add README"
```

Install NodeJS
```
npm init
npm install
npm run build
```
Double-click on index.js and write down in the terminal :
```
npm run build
```

## Auteur
* Aleksandr Vassilyev
* Claudian Camus
* Quentin Costes
* Julie Bonnefond